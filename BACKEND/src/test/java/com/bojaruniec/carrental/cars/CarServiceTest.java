package com.bojaruniec.carrental.cars;

import com.bojaruniec.carrental.cars.exceptions.CarNotFoundException;

import com.bojaruniec.carrental.cars.specification.SpecificationOfCar;
import com.bojaruniec.carrental.cars.specification.SpecificationService;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Optional;


import static org.assertj.core.api.Assertions.assertThatThrownBy;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class CarServiceTest {

    @Mock
    private CarRepository carRepository;
    @Mock
    private SpecificationService specificationService;

    @InjectMocks
    private CarService carService;


    @Test
    void canGetSingleCar() {
        // given
        long id = anyLong();
        SpecificationOfCar specification = new SpecificationOfCar("brand",
                "model",
                120.0,
                90.0,
                5);

        Car car = new Car("plates", specification);
        when(carRepository.findById(id)).thenReturn(Optional.of(car));
        // when
        carService.getSingleCar(id);
        // then
        verify(carRepository, times(1)).findById(id);
        verifyNoMoreInteractions(carRepository);
    }

    @Test
    void shouldThrowWhenSingleCarNotExist() {
        // given
        long id = anyLong();
        // then
        assertThatThrownBy(() -> carService.getSingleCar(id))
                .isInstanceOf(CarNotFoundException.class)
                .hasMessageContaining("Car with id %s not found", id);
    }

    @Test
    void canGetListOfCars() {
        // when
        carService.getListOfCars();
        // then
        verify(carRepository).findAll();
    }

    @Test
    void shouldAddCarWhenOnlySpecificationIdIsGiven() {
        // given
        long id = 1;
        CarDto carWithSpecificationId =
                new CarDto("plates", id);
        SpecificationOfCar specification = new SpecificationOfCar("brand",
                "model",
                120.0,
                90.0,
                5);

        when(specificationService.getSpecification(id)).thenReturn(Optional.of(specification));
        // when
        carService.addCar(carWithSpecificationId);
        // then
        verify(specificationService, times(2)).getSpecification(id);
        verify(carRepository, times(1)).save(Mockito.any(Car.class));
        verify(specificationService, times(0))
                .addSpecification(Mockito.any(SpecificationOfCar.class));
    }

    @Test
    void shouldAddNewCarWhenOnlySpecificationIsGiven() {
        // given
        long id = anyLong();
        SpecificationOfCar specification = new SpecificationOfCar("brand",
                "model",
                120.0,
                90.0,
                5);
        CarDto carWithSpecification =
                new CarDto("plates", specification);
        // when
        carService.addCar(carWithSpecification);
        // then
        verify(specificationService, times(1)).getSpecification(id);
        verify(specificationService, times(1)).addSpecification(specification);
        verify(carRepository, times(1)).save(Mockito.any(Car.class));
    }

    @Test
    void shouldThrowWhenSpecificationIdAndSpecificationIsNotGiven() {
        //given
        CarDto carToAdd = new CarDto();
        //then
        assertThatThrownBy(() -> carService.addCar(carToAdd))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Cannot add car by given arguments");

    }
}