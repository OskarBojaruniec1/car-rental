package com.bojaruniec.carrental.cars.specifications;


import com.bojaruniec.carrental.images.ImageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SpecificationService {

    private final SpecificationRepository specificationRepository;
    private final ImageService imageService;



    public Optional<SpecificationOfCar> getSpecification(long id) {
        return specificationRepository.findById(id);
    }

    public SpecificationOfCar addSpecification(SpecificationOfCar specification) {
        return specificationRepository.save(specification);
    }

    public List<SpecificationOfCar> getListOfSpecification() {
        return specificationRepository.findAll();
    }
}
