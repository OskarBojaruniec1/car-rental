package com.bojaruniec.carrental.images;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.zip.DataFormatException;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class ImageController {

    private final ImageService imageService;

    @PostMapping("/images")
    public Image addImage(@RequestParam("imageFile")MultipartFile imageFile) {
        return imageService.addImage(imageFile);
    }

    @GetMapping("/images/{imageName}")
    public Image getImage(@PathVariable("imageName") String imageName) throws DataFormatException, IOException {
        return imageService.getImage(imageName);
    }
    @GetMapping("/images")
    public List<Image> getListOfImages() {
        return imageService.getListOfImages();
    }
}
