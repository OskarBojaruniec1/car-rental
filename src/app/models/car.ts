import { Specification } from "./specification";

export class Car {

    id: number;
    licensePlate: string;
    specification: Specification;
}
